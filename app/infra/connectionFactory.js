var mysql = require('mysql');
var pool = null;

function _criaPool() {

  if(!process.env.NODE_ENV) {

    pool = mysql.createPool({
      connectionLimit: 100,
      host: 'localhost',
      user: 'root',
      password: 'silva!$!$',
      database: 'casadocodigo_nodejs'
    });
  }

  if(process.env.NODE_ENV == 'test') {

    pool = mysql.createPool({
      connectionLimit: 100,
      host: 'localhost',
      user: 'root',
      password: 'silva!$!$',
      database: 'casadocodigo_nodejs_test'
    });
  }

  if(process.env.NODE_ENV == 'production') {

    var urlDeConexao = process.env.CLEARDB_DATABASE_URL;
    var grupos = urlDeConexao.match(/mysql:\/\/(.*):(.*)@(.*)\/(.*)\?reconnect=true/);

    pool = mysql.createPool({
      connectionLimit: 10,
      host: grupos[3],
      user: grupos[1],
      password: grupos[2],
      database: grupos[4]
    });
  }

  //Se a fila ta cheia
  pool.on('enqueue', function() {
    console.error('watting for available connection slot');
  });

}
_criaPool();

var createDBConnection = function(callback) {

  return pool.getConnection(function(err, connection) {

    if(err) {

      console.log('Erro ao obter a conexão', err);
      
      pool.end(function(err) {
        
        if(err) {
          console.log('erro ao terminar o pool', err);
        }

         //Recria o pool
        _criaPool();
      });

      return;
    }

    return callback(null, connection);
  });

}

//wrapper
module.exports = function () {
  return createDBConnection;
}