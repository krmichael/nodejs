module.exports = function(app) {
  
  app.get('/', function(req, res) {

    var ProdutosDAO = new app.infra.ProdutosDAO(app);//a connection é passada para o app

    ProdutosDAO.lista(function(err, results) {

      err ? (res.render('erros/500'), console.log(err)) :
      res.format({
        html: () => res.render('home', { lista: results }),
        json: () => res.json(results)
      });

    });
  });

  app.get('/livros', function(req, res) {

    var ProdutosDAO = new app.infra.ProdutosDAO(app);

   ProdutosDAO.lista(function(err, results) {
    
    err ? (res.render('erros/500'), console.log(err)) :
    res.format({
       html: () => res.render('livros/lista', { lista: results }),
       json: () => res.json(results)
     });

   });
  });

  app.get('/livros/form', function(req, res) {
    res.render('livros/form', { produto: {}, errosValidacao: {} });
  });

  app.post('/livros', function(req, res) {
    
    var produto = req.body;

    req.assert('titulo', 'O titulo é obrigatório').notEmpty();
    req.assert('preco', 'Formato inválido').isFloat();

    var erros = req.validationErrors();

    if(erros) {
      res.format({
        html: () => res.status(400).render('livros/form', { errosValidacao: erros, produto: produto }),
        json: () => res.status(400).json(erros)
      });
      return;
    }

    var ProdutosDAO = new app.infra.ProdutosDAO(app);

    ProdutosDAO.salva(produto, (err, results) =>
      err ? (res.render('erros/500'), console.log(err)) : res.redirect('/livros'));
  });

}