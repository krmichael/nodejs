module.exports = function(app) {

  app.get('/promocoes/form', function(req, res) {

    var ProdutosDAO = new app.infra.ProdutosDAO(app);

    ProdutosDAO.lista(function(err, results) {

      err ? console.log(err) :
      res.format({
        html: () => res.render('promocoes/form', { lista: results }),
        json: () => res.json(results)
      });

    });
  });

  app.post('/promocoes', function(req, res) {

    var promocao = req.body;
    app.get('io').emit('novaPromocao', promocao);
    res.redirect('promocoes/form');
  });

}