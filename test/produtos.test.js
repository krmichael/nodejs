var app = require('../config/express')();
var request = require('supertest')(app);

describe('#GET /produtos', function() {

  this.beforeEach(function(done) {

    var conn = app.infra.connectionFactory();
        conn.query('delete from livros', function(err, results) {

          if(!err) {
            done();
          }
        });
  });

  it('listagem dos produtos em formato json', function(done) {
    
    request.get('/produtos')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);

  });

  it('Cadastro de um novo produto com dados inválidos', function(done) {

    let produto = {
      titulo: "",
      descricao: "descrição",
      preco: 30
    };

    request.post('/produtos')
      .send(produto)
      .expect(400, done);

  });

  it('Cadastro de um novo produto com dados válidos', function(done) {
    
    let produto = {
      titulo: 'Execultando testes com mocha em nodejs',
      descricao: 'Aprenda a usar o mocha e escreva códigos mais assertivos',
      preco: 40
    };

    request.post('/produtos')
      .send(produto)
      .set('Accept', 'application/json')
      .expect(302, done);

  });

});