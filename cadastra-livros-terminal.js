var http = require('http');

var options = {
  hostname: 'localhost',
  port: 3000,
  path: '/produtos',
  method: 'post',
  headers: {
    'Accept': 'application/json',
    'Content-type': 'application/json'
  }
}

var client = http.request(options, function(res) {

  console.log(res.statusCode);
  res.on('data', function(body) {
    console.log('body: '+body);
  });

});

var livro = {
  titulo: 'Mais sobre nodejs',
  descricao: 'nodejs, javascript e um pouco sobre http',
  preco: 45
}

client.end(JSON.stringify(livro));